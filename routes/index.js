var express = require('express');
var router = express.Router();

/* GET home page and pass data to front end*/
router.get('/', function(req, res, next) {
  res.render('index',
      {
        title: 'Dog Adoption'
      });
});

module.exports = router;
