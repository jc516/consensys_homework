var createError  = require('http-errors');
var express      = require('express');
var path         = require('path');
var cookieParser = require('cookie-parser');
var logger       = require('morgan');
var fs           = require("fs");
var bodyParser   = require('body-parser');

const { check, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');


// LOAD: front end pages to include
var indexRouter = require('./routes/index');

var app = express();
app.listen(3001);//same port as proxy set in nginx
app.use('/public', express.static('public'));//static file setup
app.use(bodyParser.urlencoded({ extended: true }));// Grab data from web forms

// ACTION: Get JSON list of all dogs from API
app.get('/listAllDogs', function (req, res) {
    fs.readFile( __dirname + "/" + "dogs.json", 'utf8', function (err, data) {
        console.log( data );
        res.end( data );
    });
});

// Get details of a dog when filtering by age or size from API
app.post('/search', [
    check('age').isInt().withMessage('Must be a number'),
    check('size').isString().withMessage('example: S, M, 3XL')
],function (req, res) {

    // CHECK: Extract the validation errors from a request.
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    } else {

        // ACTION: Mimic db connection by getting data from JSON
        fs.readFile( __dirname + "/" + "dogs.json", 'utf8', function (err, data) {

            // LOAD: Valid data
            var dataJSONResponse   = JSON.parse(data);
            var ageFormSubmission  = req.body.age;
            var sizeFormSubmission = req.body.size;
            var ageResponseJSON    = [];
            var responseJSON       = [];

            // ACTION: Get age filter
            numDogs = dataJSONResponse.dogs.length;
            for(var i = 0; i < numDogs; i++){
                if(dataJSONResponse.dogs[i].age <= ageFormSubmission){
                    ageResponseJSON.push(dataJSONResponse.dogs[i]);
                }
            }

            // ACTION: Get size filter
            numAgeDogs = ageResponseJSON.length;
            if(sizeFormSubmission === "Any"){
                responseJSON = ageResponseJSON;
            }else{
                for(var j = 0; j < numAgeDogs; j++){
                    if(ageResponseJSON[j].size === sizeFormSubmission){
                        responseJSON.push(ageResponseJSON[j]);
                    }
                }
            }
            res.render('index', {
                title: 'Dog Adoption',
                responseJSON: responseJSON
            });
        });
    }
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
