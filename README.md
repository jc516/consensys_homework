# consensys_homework
- X You will need to create both the front-end and back-end using Javascript.
- X The front end can use any javascript toolkit/library that you wish.
- X The back end can be either a / or a GraphQL API but should be written in NodeJS.
- X You can create a database if you wish, or you can use static JSON files to simulate the process of getting data from a database (there are tools like Mockaroo that can help with this).
- X The data itself doesn't matter, it could be a list of bands and their songs, a list of animals and their species-classifications, a list of car makers and their car models... whatever you choose!
- X Allow the data to be filtered by the user in at least one way (filter by dog size and age).
- X All code should be properly linted, tested, and built to a standard. The packages you choose for these do not matter.
- x Once you are done, push the code to a repository of your choice and send us the link.
Good luck and feel free to reach out with any questions.


The code allows the user to filter results on a list of dogs
by age and size, and return the results. 



